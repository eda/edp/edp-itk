package edlab.eda.edp.itk;

import edlab.eda.edp.itk.device.CallbackEngine;
import edlab.eda.edp.itk.nl.Formatter;
import edlab.eda.edp.itk.nl.NetlistingEngine;
import edlab.eda.edp.itk.technology.TechnologyIntegration;

public final class CustomTechnologyIntegration extends TechnologyIntegration {

  private static final String SIMULATOR = "spectre";

  @Override
  public boolean hasNetlistingEngine(final String simulator) {
    return SIMULATOR.equals(simulator);
  }

  public NetlistingEngine getNetlistingEngine(final String simulator,
      final Formatter formatter) {

    if (SIMULATOR.equals(simulator)) {
      return new CustomNetlistingEngine(formatter);
    }

    return null;
  }

  @Override
  public boolean hasCallbackEngine(final String libName,
      final String cellName) {
    // TODO
    return false;
  }

  @Override
  public CallbackEngine getCallbackEngine(final String libName,
      final String cellName) {
    return null;
  }
}