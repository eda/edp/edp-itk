package edlab.eda.edp.itk;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edlab.eda.edp.itk.nl.NetlistingEngine;
import edlab.eda.edp.itk.technology.TechnologyIntegration;

class TechnologyIntegrationTest {

  @Test
  void test() {

    TechnologyIntegration techIntegration = new CustomTechnologyIntegration();

    assertNotNull(techIntegration);
    assertTrue(techIntegration.hasNetlistingEngine("spectre"));
    assertFalse(techIntegration.hasNetlistingEngine("ngspice"));

    NetlistingEngine engine;

    engine = techIntegration.getNetlistingEngine("ngspice",
        new CustomFormatter());

    assertNull(engine);

    engine = techIntegration.getNetlistingEngine("spectre",
        new CustomFormatter());

    assertNotNull(engine);

  }
}