package edlab.eda.edp.itk.technology;

import edlab.eda.edp.itk.device.CallbackEngine;
import edlab.eda.edp.itk.nl.Formatter;
import edlab.eda.edp.itk.nl.NetlistingEngine;

/**
 * Integrator Tool-Kit for customizing the semiconductor technology. You must
 * derive this class to create your own technology integration.
 */
public abstract class TechnologyIntegration {

  /**
   * Check if the {@link TechnologyIntegration} can return a
   * {@link NetlistingEngine} for a simulator with a given name
   * 
   * @param simulator name of simulator, e.g. "spectre"
   * @return <code>true</code> when the {@link TechnologyIntegration} can return
   *         a {@link NetlistingEngine} for the simulator , <code>false</code>
   *         otherwise.
   */
  public abstract boolean hasNetlistingEngine(final String simulator);

  /**
   * Get a {@link NetlistingEngine} for simulator
   * 
   * @param simulator name of simulator, e.g. "spectre"
   * @param formatter Netlisting formatter
   * @return netlisting engine
   */
  public abstract NetlistingEngine getNetlistingEngine(final String simulator,
      final Formatter formatter);

  /**
   * Identify if a primitive has a callback engine
   * 
   * @param libraryName Library name
   * @param cellName    Cell name
   * @return <code>true</code> when the device has a {@link CallbackEngine},
   *         <code>false</code> otherwise
   */
  public abstract boolean hasCallbackEngine(final String libraryName,
      final String cellName);

  /**
   * Get a {@link CallbackEngine} of a device
   * 
   * @param librarybName Library name
   * @param cellName     Cell name
   * 
   * @return callback engine when available, <code>null</code> otherwise
   */
  public abstract CallbackEngine getCallbackEngine(final String librarybName,
      final String cellName);
}