package edlab.eda.edp.itk.nl;

/**
 * Formatter for netlisting
 */
public interface Formatter {

  /**
   * Check if a subcircuit with a given name is already added to the netlist
   * 
   * @param name name to be checked
   * @return <code>true</code> when is already a subcircuit name,
   *         <code>false</code> otherwise
   */
  public boolean isSubcircuitName(final String name);

  /**
   * Append a netlisting statement for a given instance
   * 
   * @param handle    Handle to the instance
   * @param statement Netlisting statement to be added
   * 
   * @return <code>this</code> when successful, <code>null</code> otherwise
   */
  public Formatter append(final InstanceNetlistingHandle handle,
      final String statement);

  /**
   * Append a netlisting statement for a given instance
   * 
   * @param handle  Handle to the instance
   * @param builder Builder
   * 
   * @return <code>this</code> when successful, <code>null</code> otherwise
   */
  public Formatter append(final InstanceNetlistingHandle handle,
      final NetlistStatementBuilder builder);

  /**
   * Append a named netlist statement
   * 
   * @param name      Name
   * @param statement Netlisting statement to be added
   * 
   * @return <code>this</code> when successful, <code>null</code> otherwise
   */
  public Formatter append(final String name, final String statement);

  /**
   * Append a named netlisting statement
   * 
   * @param name    Name
   * @param builder Builder
   * 
   * @return <code>this</code> when successful, <code>null</code> otherwise
   */
  public Formatter append(final String name,
      final NetlistStatementBuilder builder);

  /**
   * Append an unnamed netlist statement
   * 
   * @param statement Netlisting statement to be added
   * 
   * @return <code>this</code> when successful, <code>null</code> otherwise
   */
  public Formatter append(final String statement);

  /**
   * Append a unnamed netlisting statement
   * 
   * @param builder Builder
   * 
   * @return <code>this</code> when successful, <code>null</code> otherwise
   */
  public Formatter append(final NetlistStatementBuilder builder);

  /**
   * Append a formatter to an existing formatter
   * 
   * @param formatter Formatter to be appended
   * 
   * @return <code>this</code> when successful, <code>null</code> otherwise
   */
  public Formatter append(final Formatter formatter);

  /**
   * Get an empty netlist statement builder
   * 
   * @return builder
   */
  public NetlistStatementBuilder getStatementBuilder();

  /**
   * Get the name of the ground net, this is typically <code>0</code>
   * 
   * @return name of the net
   */
  public abstract String getGround();
}