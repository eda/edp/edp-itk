package edlab.eda.edp.itk.nl;

/**
 * Handle to a parameter property
 */
public interface ParameterProperty {

  /**
   * Get the EDP name of the parameter
   * 
   * @return name
   */
  public String getName();

  /**
   * Get the name of the parameter in the design environment
   * 
   * @return real name
   */
  public String getRealName();

  /**
   * Get the information if a parameter is nullable, i.e. can be set to
   * <code>null</code>
   * 
   * @return nullable
   */
  public boolean isNullable();
}