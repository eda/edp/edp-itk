package edlab.eda.edp.itk.device;

/**
 * Engine that is able to
 */
public interface CallbackEngine {

  /**
   * Identify if the engine is able to handle the trigger of a parameter
   * 
   * @param parameter Parameter that is triggered
   * @return <code>true</code> when the engine is able to handle the trigger,
   *         <code>false</code> otherwise
   */
  public boolean canHandleCallback(final String parameter);

  /**
   * Handle the callback of a {@link Device}
   * 
   * @param device    Device
   * @param parameter name of triggered parameter
   * @return this
   * @throws UnableToTriggerCallbackExeption when the callback could not be
   *                                         executed
   */
  public CallbackEngine handleCallback(final Device device,
      final String parameter) throws UnableToTriggerCallbackExeption;
}