/**
 * This module contains functions for executing callbacks of primitive devices
 * in a Process Design Kit
 */
package edlab.eda.edp.itk.device;