package edlab.eda.edp.itk.device;

/**
 * Exception that is thrown when triggering of the callback failed
 */
public final class UnableToTriggerCallbackExeption extends Exception {

  private static final long serialVersionUID = 5569598843290773634L;

  /**
   * Device where the parameter is defined
   */
  public final Device device;

  /**
   * Name of the parameter
   */
  public final String parameterName;

  /**
   * Create a exception
   * 
   * @param device             Device
   * @param triggeredParameter Parameter that is triggered
   * @param message            Message
   */
  public UnableToTriggerCallbackExeption(final Device device,
      final String triggeredParameter, final String message) {
    this.device = device;
    this.parameterName = triggeredParameter;
  }
}